# Boomstick v3.0

## What is Boomstick?

It's a 12-gauge, double-barrelled Remington. S-Mart's top of the line.

Wait, that's not right.

Boomstick is a tool to destroy data on basically any Windows or Linux PC.

## What can Boomstick do?

Boomstick can do anything, as long as "anything" means "destroy data in a reasonably reliable way."

## How does it do that?

For NVMe devices, Boomstick will use NVMe Format on the disk.

For ATA devices, Boomstick will attempt to use the ATA SANITIZE_CRYPTO_SCRAMBLE and SANITIZE_BLOCK_ERASE commands. If SANITIZE isn't supported, it'll try ATA SECURE_ERASE.

After those are complete for all disks, it will attempt to run a secure discard (TRIM) operation followed by a regular discard. Next, it writes three passes of random data to the drive. Finally, it runs another secure discard followed by a normal discard.

In all of these cases, you trust that your drive completely gets rid of its stored data when these operations are run on it. SSDs might accept the secure discard operation, for example, but still keep some data on the disk. The only way to verify this would be to take the NAND flash chips off the SSD's motherboard and communicate with them directly to find your data (or a similar process with a hard drive's platters and some specialized tools). The three passes of random data will probably deal with anything that remains. Still, if you don't trust your drive, you probably want to destroy it physically.

## Where do I get it?

Unfortunately, I don't know how to make GitLab CI build and serve completed Boomstick images for your downloading pleasure. For now, you'll have to build it yourself.

You need a Linux system with working KVM, your account needs access to the `/dev/kvm` device (you can probably get this with `usermod -aG kvm $(whoami)` and re-logging if you're on Ubuntu), and Docker or Podman installed. Clone this repository to your computer. Then, run the appropriate command for your container frontend from the repository's directory:

```
docker run --net=host --rm --interactive --tty \
        --device /dev/kvm \
        --user $(id -u) \
        --workdir /recipes \
        --group-add=$(getent group kvm | cut -d : -f 3) \
        --mount "type=bind,source=$(pwd),destination=/recipes" --security-opt label=disable \
        docker.io/godebos/debos boomstick.yaml
```

```
podman run --net=host --rm --interactive --tty \
        --device /dev/kvm \
        --workdir /recipes \
        -v $(pwd):/recipes:z \
        --group-add keep-groups \
        --security-opt label=disable \
        docker.io/godebos/debos boomstick.yaml
```

You'll find a new `boomstick.img` file in your current directory. Use [Etcher](https://etcher.io) or your favorite image-writing tool to write it to a USB drive. Boot your newly-crafted Boomstick in any computer that you wish to be devoid of data.

## What **doesn't** it do?

These are things that Boomstick could do but doesn't right now. I'd appreciate your help with these.

Boomstick does not verify that the data is gone as far as the operating system can tell by reading the media back after it erases. That is an exercise left to you.

For any more potential issues that could/should be resolved, see [the Issues tab of this repository](-/issues).

## What **can't** it do?

These are things that Boomstick cannot do and that I would likely not accept any contributions to change.

Boomstick cannot provide a guarantee that your data is gone. For practical reasons for this limitation, please see ["Complications" in the Wikipedia article on data remanence](https://en.wikipedia.org/wiki/Data_remanence#Complications). For legal reasons, this should serve as your disclaimer. Make sure you balance your threat model against your mitigation strategy. If your data is likely to interest people with big budgets, the only way to be sure it's gone is to physically destroy (crush, shred, and/or incinerate) the storage media it lives on.

Boomstick cannot provide lots of settings for how you want your data destroyed. If you have a disk that supports SANITIZE operations but you want to use SECURE ERASE instead, you're out of luck. I don't want to make something super complex since that flies in the face of the easy-to-use nature of Boomstick. Too many code paths make the result harder to test. I would, however, accept the suggestion that Boomstick should use *all* of the supported erase methods in every case.

## Who made Boomstick?

Hi! I'm [May](https://maydur.st). I made Boomstick originally. If you wanted to help, I'd appreciate it and add you to this README section.

## Why does Boomstick exist?

I created Boomstick out of frustration in a perfect storm:

* DBAN hasn't been updated for a long time.
* DBAN doesn't work on UEFI systems, and it especially doesn't work on Secure Boot-enabled systems.
* I didn't want to mess with the firmware setup on every machine to enable and disable Legacy boot. Some machines didn't even have Legacy boot.
* I didn't want to pay for a commercial solution.
* I was getting tired of typing the commands to wipe devices over and over again or clicking the buttons in Parted Magic.

## What if Boomstick doesn't work for me?

If you run into a bug or limitation that isn't covered by the "What can't it do" section above, please let me know via one of the following methods.

If you file an issue on [the Issues tab of this repository](-/issues), I might be able to help out. But I created Boomstick for a specific purpose it has served, so I can't guarantee I'll ever answer you.

The license of Boomstick allows you to modify it to make it better for your uses. If you do, I'd appreciate it if you let me know. You could even send a Merge Request, and I'll try my best to consider it. I can't guarantee I'll ever answer you or that I'll merge your changes, but I'll almost definitely see it and be very happy that you took the time out of your day to help.

## License

This repository is provided under the MIT license. See the [LICENSE](LICENSE) file for more details. The `/etc/default/grub` file was taken from Debian and modified. It may be under another license, but I'm not sure. A built copy of Boomstick has many more licenses. You can find them in the `/usr/share/doc/*/copyright` files on a built Boomstick image.
